
<div class="footer">
	<div class="c-footer1">
		<div class="c-footer1__info">
			<li><a href="#">ホーム </a></li>
			<li><a href="#">お知らせ </a></li>
			<li><a href="#">株式会社team sakata </a></li>
			<li><a href="#">一般社団法人SHIEN </a></li>
			<li><a href="#">一般社団法人team shien </a></li>
			<li><a href="#">NPO法人ライフサポートさくら </a></li>
			<li><a href="#">会社案内</a></li>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="c-footer2">
		<div class="c-footer2__info">
			<div class="c-footer2__info__left">
				<h1>株式会社team sakata、一般社団法人SHIEN、一般社団法人team shien </h1>
				<p>〒156-0042　東京都世田谷区羽根木1-13-9-201</p>
				<p>TEL：03-6379-5484（代表）</p>
			</div>
			<div class="c-footer2__info__right">
				<h1>NPO法人ライフサポートさくら</h1>
				<p>〒154-0021　東京都世田谷区豪徳寺1-21-5 ヴィラ・ゴートク1-E</p>
				<p>TEL：03-3439-3977</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="c-footer3">
		<p>COPYRIGHT© Team Egao Group　 ALL RIGHTS RESERVED.</p>
	</div>
</div>


</body>
</html>