<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title>company</title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

<div class="c-maintitle1">
	<img src="../assets/image/company/maintitle1.png" alt="">
	<div class="c-maintitle1__text">
		<h1>会社案内</h1>
		<h2>Company Data</h2>
	</div>
</div>

<div class="c-breadcrumbs">
	<div class="c-breadcrumbs__text">
		<li>
			<a href="#">HOME</a>
			> 会社案内
		</li>
	</div>
</div>

<div class="p-company">
	<div class="p-company1">
		<div class="l-container">
			<div class="c-nav1">
				<div class="c-nav1--colum">
					<a href="#">
						<p>
							株式会社team sakata
						</p>
					</a>
					<img src="../assets/image/company/com-img5.png" alt="">
					<div class="clearfix"></div>
				</div>

				<div class="c-nav1--colum">
					<a href="#">
						<p>
							一般社団法人SHIEN
						</p>
					</a>
					<img src="../assets/image/company/com-img5.png" alt="">
					<div class="clearfix"></div>
				</div>

				<div class="c-nav1--colum">
					<a href="#">
						<p>
							一般社団法人team shien
						</p>
					</a>
					<img src="../assets/image/company/com-img5.png" alt="">
					<div class="clearfix"></div>
				</div>

				<div class="c-nav1--colum">
					<a href="#">
						<p>
							NPO法人ライフサポートさくら
						</p>
					</a>
					<img src="../assets/image/company/com-img5.png" alt="">
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div class="p-company2">
		<div class="l-container">
			<div class="c-row-company1">
				<div class="c-title3">
					<ul class="c-ul1">
						<img src="../assets/image/company/com-img1.png" alt="">
						<p>株式会社team sakata</p>
					</ul>
				</div>
				<div class="c-table1">
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>運営主体</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>株式会社 team sakata</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>所在地</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>〒156-0042　東京都世田谷区羽根木1-13-9-201</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>代表者名</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>阪田　祐治</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>TEL</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3323-2495</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>FAX</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3327-7166</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>設立</h1>
						</div>
						<div class="c-table1__tr__right">
							<p></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

			<div class="c-row-company1">
				<div class="c-title3">
					<ul class="c-ul1">
						<img src="../assets/image/company/com-img2.png" alt="">
						<p>一般社団法人SHIEN</p>
					</ul>
				</div>
				<div class="c-table1">
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>運営主体</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>一般社団法人SHIEN</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>所在地</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>〒156-0042　東京都世田谷区羽根木1-13-9-201</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>代表者名</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>阪田　祐治</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>TEL</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3323-2495</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>FAX</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3327-7166</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>設立</h1>
						</div>
						<div class="c-table1__tr__right">
							<p></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

			<div class="c-row-company1">
				<div class="c-title3">
					<ul class="c-ul2">
						<img src="../assets/image/company/com-img3.png" alt="">
						<p>一般社団法人team shien</p>
					</ul>
				</div>
				<div class="c-table1">
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>運営主体</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>一般社団法人team shien</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>所在地</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>〒156-0042　東京都世田谷区羽根木1-13-9-201</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>代表者名</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>阪田　祐治</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>TEL</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-6379-5484</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>FAX</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3327-7166</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>設立</h1>
						</div>
						<div class="c-table1__tr__right">
							<p></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>


			<div class="c-row-company2">
				<div class="c-title3">
					<ul class="c-ul3">
						<img src="../assets/image/company/com-img4.png" alt="">
						<p>NPO法人ライフサポートさくら</p>
					</ul>
				</div>
				<div class="c-table1">
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>運営主体</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>NPO法人ライフサポートさくら</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>所在地</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>〒154-0021　東京都世田谷区豪徳寺1-21-5 ヴィラ・ゴートク1-E</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>代表者名</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>阪田　祐治</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>TEL</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3439-3977</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>FAX</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3439-3978</p>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>設立</h1>
						</div>
						<div class="c-table1__tr__right">
							<p></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="../assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
