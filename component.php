<div class="c-maintitle1">
	<img src="assets/image/company/maintitle1.png" alt="">
	<div class="c-maintitle1__text">
		<h1>会社案内</h1>
		<h2>Company Data</h2>
	</div>
</div>

<div class="c-breadcrumbs">
	<div class="c-breadcrumbs__text">
		<li>
			<a href="#">HOME</a>
			> 一般社団法人team shien
		</li>
	</div>
</div>

<!--  -->
<div class="l-container">
	
	<div class="c-title1">
		<h1>チームエガオグループ（T.E.G Team Egao Group）</h1>
		<p>About</p>
	</div>
<!--  -->
	<div class="c-title2">
		<p>ホームページをリニューアルしました。</p>
	</div>
<!--  -->
	<div class="c-title3">
		<ul class="c-ul1">
			<img src="assets/image/company/com-img1.png" alt="">
			<p>株式会社team sakata</p>
		</ul>
	</div>
<!--  -->
	<div class="c-title4">
		<h1>介護保険ケアプラン（事業所番号 1371212207）</h1>
	</div>
<!--  -->
	<div class="c-title5">
		<img src="assets/image/teamshien/teamsakata-img1.png" alt="">
		<p>team shien</p>
		<div class="clearfix"></div>
	</div>
<!--  -->
	<div class="c-title6">
		<h2>成年後見制度とは</h2>
	</div>
<!--  -->
	<div class="c-title7">
		<h2>YYYY/MM/DD</h2>
	</div>
<!--  -->
	<div class="c-title100">
		<div class="c-btn1">
			<a href="#">Facebook</a>
		</div>
		<div class="c-title8">
			<h1>お知らせ</h1>
			<p>News</p>
		</div>
		<div class="clearfix"></div>
	</div>
<!--  -->
	<div class="c-table1">
		<div class="c-table1__tr">
			<div class="c-table1__tr__left">
				<h1>運営主体</h1>
			</div>
			<div class="c-table1__tr__right">
				<p>株式会社 team sakata</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

<!--  -->
	<div class="c-table1--color2">
		
	</div>

<!--  -->
	<div class="c-list1">
		<li>
			<img src="assets/image/teamsakata/teamsakata-img1.png" alt="">
			京王線「代田橋」駅　徒歩6分
		</li>
	</div>

<!--  -->
	<div class="c-list2">
		<ul>
			<li><img src="assets/image/top/img1.png" alt=""><span>2017/10/20</span>イベント情報ページを更新しました。</li>
		</ul>
	</div>

<!--  -->
	<div class="c-info1">
		<h1>このような法人様におすすめ</h1>
		<li>
			<img src="assets/image/teamsakata/teamsakata-img2.png" alt="">
			介護事業の事が全くわからない。
		</li>
		<li>
			<img src="assets/image/teamsakata/teamsakata-img2.png" alt="">
			一人では不安だから一緒に事業計画から安定運営までサポートして欲しい。
		</li>
		<li>
			<img src="assets/image/teamsakata/teamsakata-img2.png" alt="">
			障害福祉サービスを取り組みたい。
		</li>
		<li>
			<img src="assets/image/teamsakata/teamsakata-img2.png" alt="">
			更生保護事業に興味がある。
		</li>
	</div>

<!--  -->

	<div class="c-info2">
		<h1>お問い合わせ先</h1>
		<p>月～土曜日 9:00～18:00 / TEL.03-6379-4343</p>
	</div>

<!--  -->
	<div class="c-info2">
		<div class="c-info2--color2">
			<h1>お問い合わせ先</h1>
			<p>月～金曜日 9:00～18:00 / TEL.03-6379-5484</p>
		</div>
	</div>

<!--  -->
	<div class="c-info3">
		<div class="c-info3--left">
			<h1>チームエガオグループで一緒に働きませんか？</h1>
			<p>ご興味のある方はお電話にてご連絡ください。</p>
			<img src="assets/image/top/img12.png" alt="">
		</div>
		<div class="c-info3--right">
			<li>
				<img src="assets/image/top/img13.png" alt="">
				<span class="padd-right">
					株式会社 team sakata
				</span>
			</li>
			<li>
				<img src="assets/image/top/img14.png" alt="">
				<span class="padd-right">
					一般社団法人 SHIEN
				</span>
			</li>
			<li>
				<img src="assets/image/top/img15.png" alt="">
				<span>
					一般社団法人  team shien
				</span>
			</li>
			<li>
				<img src="assets/image/top/img16.png" alt="">
				<span class="padd-right">
					NPO法人  ライフサポートさくら
				</span>
			</li>
		</div>
		<div class="clearfix"></div>
	</div>

<!--  -->

	<div class="c-mapBlock">
		<div class="c-map1">
			<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:400px;width:950px;'><div id='gmap_canvas' style='height:400px;width:950px;'></div><div><small><a href="https://www.embedgooglemaps.com">embedgooglemaps.com</a></small></div><div><small><a href="https://mrdiscountcode.hk/zuzu-hotels/">http://mrdiscountcode.hk/zuzu-hotels/</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(35.669484458026275,139.65770790670683),mapTypeId: google.maps.MapTypeId.TERRAIN};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(35.669484458026275,139.65770790670683)});infowindow = new google.maps.InfoWindow({content:'<strong>Title</strong><br>京王線 代田橋<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
		</div>
		<a href="https://www.google.co.jp/maps/place/%E4%BB%A3%E7%94%B0%E6%A9%8B%E9%A7%85/@35.6695161,139.6582014,16z/data=!4m5!3m4!1s0x6018f3055dada5f3:0x8e544c47758a09cd!8m2!3d35.671071!4d139.659387">詳しい地図を見る</a>
		<div class="c-list1">
			<li>
				<img src="assets/image/teamsakata/teamsakata-img1.png" alt="">
				京王線「代田橋」駅　徒歩6分
			</li>
		</div>
	</div>


<!--  -->

	<div class="c-nav1">
		<div class="c-nav1--colum">
			<p>
				株式会社team sakata
			</p>
			<img src="assets/image/company/com-img5.png" alt="">
			<div class="clearfix"></div>
		</div>

		<div class="c-nav1--colum">
			<p>
				一般社団法人SHIEN
			</p>
			<img src="assets/image/company/com-img5.png" alt="">
			<div class="clearfix"></div>
		</div>

		<div class="c-nav1--colum">
			<p>
				一般社団法人team shien
			</p>
			<img src="assets/image/company/com-img5.png" alt="">
			<div class="clearfix"></div>
		</div>

		<div class="c-nav1--colum">
			<p>
				NPO法人ライフサポートさくら
			</p>
			<img src="assets/image/company/com-img5.png" alt="">
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>

<!--  -->

	<div class="c-nav2">
		<div class="c-nav2__tr">
			<div class="c-nav2__tr__colum">
				<div class="c-nav2__tr__colum__back">
					<img src="assets/image/top/img7.png" alt="">
				</div>
				<div class="c-nav2__tr__colum__text">
					<a href="#">
						<img src="assets/image/top/img2.png" alt="">
						<p>株式会社team sakata</p>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

<!--  -->

	<div class="c-btn1">
		<a href="#">Facebook</a>
	</div>

<!--  -->

	<div class="c-text1">
		<p>
			テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。
		</p>
	</div>

<!--  -->

	<div class="c-entry1">
		<div class="c-entry1__left">
			<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
			<p>画像を1枚挿入できます。</p>
		</div>
		<div class="c-entry1__right">
			<p>イメージ画像</p>
		</div>
		<div class="clearfix"></div>
	</div>

<!--  -->

	<div class="l-flame1">
		<div class="l-flame1__left">
			<p>看護師・社会福祉士・精神保健福祉士・介護福祉士の資格を持った福祉のスペシャリストが介護保険利用のためのケアプランを作成を承ります。</p>
			<h6>介護保険の利用に関わらず、福祉医療全般のご相談を無料にて承ります。</h6>
			<div class="c-info2">
				<h1>お問い合わせ先</h1>
				<p>月～土曜日 9:00～18:00 / TEL.03-6379-4343</p>
			</div>
		</div>
		<div class="l-flame1__right">
			<img src="assets/image/teamshien/teamshien-img1.png" alt="">
		</div>
		<div class="clearfix"></div>
	</div>

</div>





<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>



