<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

<div class="c-maintitle1">
	<img src="../assets/image/news/maintitle2.png" alt="">
	<div class="c-maintitle1__text">
		<h1>お知らせ</h1>
		<h2>News</h2>
	</div>
</div>

<div class="c-breadcrumbs">
	<div class="c-breadcrumbs__text">
		<li>
			<a href="#">HOME</a>
			> お知らせ
		</li>
	</div>
</div>
<div class="p-news">
	<div class="p-news1">
		<div class="l-container">
			<div class="c-row1">
				<div class="c-title2">
					<p>ホームページをリニューアルしました。</p>
				</div>

				<div class="c-title7">
					<h2>YYYY/MM/DD</h2>
					<div class="clearfix"></div>
				</div>

				<div class="c-entry1">
					<div class="c-entry1__left">
						<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
						<p>画像を1枚挿入できます。</p>
					</div>
					<div class="c-entry1__right">
						<p>イメージ画像</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="c-row1">
				<div class="c-title2">
					<p>ホームページをリニューアルしました。</p>
				</div>

				<div class="c-title7">
					<h2>YYYY/MM/DD</h2>
					<div class="clearfix"></div>
				</div>

				<div class="c-entry1">
					<div class="c-entry1__left">
						<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
						<p>画像を1枚挿入できます。</p>
					</div>
					<div class="c-entry1__right">
						<p>イメージ画像</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="c-row2">
				<div class="c-title2">
					<p>ホームページをリニューアルしました。</p>
				</div>

				<div class="c-title7">
					<h2>YYYY/MM/DD</h2>
					<div class="clearfix"></div>
				</div>

				<div class="c-entry1">
					<div class="c-entry1__left">
						<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
						<p>画像を1枚挿入できます。</p>
					</div>
					<div class="c-entry1__right">
						<p>イメージ画像</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="../assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
