<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>


<div class="c-maintitle1">
	<img src="../assets/image/sakura/maintitle3.png" alt="">
	<div class="c-maintitle1__text">
		<h1>NPO法人ライフサポートさくら</h1>
		<h2>About</h2>
	</div>
</div>
<div class="c-breadcrumbs">
	<div class="c-breadcrumbs__text">
		<li>
			<a href="#">HOME</a>
			> NPO法人ライフサポートさくら
		</li>
	</div>
</div>

<div class="p-sakura p-content1">
	<div class="p-sakura1">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1 class="c-narrow">介護予防・日常生活総合支援 ～住民主体型・地域デイサービス～</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>地域デイサービス　（　さくらとゆかいな仲間たち　）</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p>みなさんで一緒にごはんを作って食べて、おしゃべりやちょっとした運動をして楽しい時間を一緒に過ごしませんか？</p>
						<br>
						<div class="c-title6">
							<h2>参加対象者</h2>
						</div>
						<p>要支援１・２及びチェックリスト該当者（65歳以上）</p>
						<br>
						<div class="c-title6">
							<h2>参加料</h2>
						</div>
						<p>食事代：500円（イベントの材料費別途徴収）</p>
						<div class="c-info2">
							<h1>お問い合わせ先</h1>
							<p>毎週水曜日10:00～13:00 / TEL.03-3439-3977</p>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/sakura/sakura-img1.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-sakura2">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>ノーマライゼーションギャラリー</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>team さくら</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p>障害の有無を意識しなくても生活ができる社会の実現を目指す考え方を共有するフリースペースです。</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/sakura/sakura-img2.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-sakura3">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>お問い合わせ先</h1>
				</div>
				<div class="c-table1">
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>電話番号</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3323-2495</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>メールアドレス</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>team-sakata@solid.ocn.ne.jp</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-sakura4">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>求人案内</h1>
				</div>
				<div class="c-text1">
					<p>
						テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="p-sakura5">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>アクセスマップ</h1>
				</div>
				<div class="c-mapBlock">
					<div class="c-map1">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1574.8464034822598!2d139.64682381128097!3d35.652719133993344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f39e89d04f03%3A0xee26a0f25f9b58ab!2zSmFwYW4sIOOAkjE1NC0wMDIxIFTFjWt5xY0tdG8sIFNldGFnYXlhLWt1LCBHxY10b2t1amksIDEgQ2hvbWXiiJIyMeKIkjUsIOODtOOCo-ODqeOCtOODvOODiOOCrw!5e0!3m2!1sen!2sus!4v1507629027059" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<a href="https://www.google.com/maps/place/Japan,+%E3%80%92154-0021+T%C5%8Dky%C5%8D-to,+Setagaya-ku,+G%C5%8Dtokuji,+1+Chome%E2%88%9221%E2%88%925,+%E3%83%B4%E3%82%A3%E3%83%A9%E3%82%B4%E3%83%BC%E3%83%88%E3%82%AF/@35.6527191,139.6468238,18.04z/data=!4m5!3m4!1s0x6018f39e89d04f03:0xee26a0f25f9b58ab!8m2!3d35.652936!4d139.6477798?hl=en-US1">詳しい地図を見る</a>
					<div class="c-list1">
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							小田急小田原線「豪徳寺」駅　徒歩2分
						</li>
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							東急世田谷線「山下」駅　徒歩3分
						</li>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="../assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
