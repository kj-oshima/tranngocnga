<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>


<div class="c-maintitle1">
	<img src="../assets/image/shien/maintitle4.png" alt="">
	<div class="c-maintitle1__text">
		<h1>一般社団法人SHIEN</h1>
		<h2>About</h2>
	</div>
</div>
<div class="c-breadcrumbs">
	<div class="c-breadcrumbs__text">
		<li>
			<a href="#">HOME</a>
			> 一般社団法人SHIEN
		</li>
	</div>
</div>

<div class="p-shien p-content1">
	<div class="p-shien1">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>成年後見</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>成年後見制度の利用相談・支援</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<div class="c-title6">
							<h2>成年後見制度とは</h2>
						</div>
						<br>
						<p>認知症や知的障がい、精神障がいなどによって判断能力が不十分な方々は、預貯金の管理や遺産分割などの法律行為（財産管理）や、福祉サービスの利用など、日常生活での様々な契約（身上監護）などを行うことが困難であったり、悪徳商法などの被害にあうおそれがあります。成年後見制度は、このような判断能力の不十分な方々を法律的に保護し、支援する制度です。</p>
						<br>
						<div class="c-title6">
							<h2>相談・支援</h2>
						</div>
						<br>
						<p>電話や窓口で、成年後見制度に関する相談や成年後見制度を利用するための手続きや申立て、費用などについて詳しく説明し、申し立て手続き等の支援をします。</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/shien/shien-img1.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-shien2">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>ケアマッサージ</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>SHIEN治療院</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p>SHIEN治療院では、ご利用者様の症状やご体調に合わせて、機能回復および機能維持を目的とした訪問医療マッサージを行っております。</p>
						<p>また、心のふれあいを大切にして、笑顔あふれる施術を心がけております。・</p>
						<br>
						<p>医療保険でマッサージが受けられることは、ご存じでしょうか？</p>
						<br>
						<p>SHIEN治療院が提供する訪問医療マッサージは、歩行困難な方を対象に医師の同意（同意書）に基づき、医療保険が適用できます。</p>
						<p>保険が適用となりますと、後期高齢者健康保険の方は実費の1割、その他の保険の方は実費の3割、障害1級2級（医療費受給者証）持参の方は全額公費負担となり、少ない自己負担でマッサージを受けていただくことができます（往療料にも保険が適用されます）。</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/shien/shien-img2.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-shien3">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>お問い合わせ先</h1>
				</div>
				<div class="c-table1">
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>電話番号</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3323-2495</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-shien4">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>求人案内</h1>
				</div>
				<div class="c-text1">
					<p>
						テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="p-shien5">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>アクセスマップ</h1>
				</div>
				<div class="c-mapBlock">
					<div class="c-map1">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
					</div>
					<a href="https://www.google.com/maps/place/1+Chome-13-9+Hanegi,+Setagaya-ku,+T%C5%8Dky%C5%8D-to+156-0042,+Japan/@35.6666557,139.6583409,17.46z/data=!4m5!3m4!1s0x6018f30e11f17561:0x9bb452e4b9254772!8m2!3d35.6667726!4d139.6595545?hl=en-US">詳しい地図を見る</a>
					<div class="c-list1">
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王線「代田橋」駅　徒歩6分
						</li>
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王井の頭線「新代田」駅　徒歩9分
						</li>
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王井の頭線「東松原」駅   徒歩9分　
						</li>
						<br>
						<li>※家庭料理のおざわが一階にあります。</li>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="../assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
