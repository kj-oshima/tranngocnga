<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>


<div class="c-maintitle1">
	<img src="../assets/image/teamsakata/mailtitle5.png" alt="">
	<div class="c-maintitle1__text">
		<h1>株式会社team sakata</h1>
		<h2>About</h2>
	</div>
</div>
<div class="c-breadcrumbs">
	<div class="c-breadcrumbs__text">
		<li>
			<a href="#">HOME</a>
			> 株式会社team sakata
		</li>
	</div>
</div>
<div class="p-teamsakata p-content1">
	<div class="p-teamsakata1">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>総合社会福祉プロデュース</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>事務所設立～運営計画</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p>事業計画の立案から、採用、育成、利用者の獲得まで、事業が安定稼働までをサポートいたします。異業種・未経験からの参入でも、しっかりとサポートいたします。</p>
						<div class="c-info1">
							<h1>このような法人様におすすめ</h1>
							<li>
								<img src="../assets/image/teamsakata/teamsakata-img2.png" alt="">
								介護事業の事が全くわからない。
							</li>
							<li>
								<img src="../assets/image/teamsakata/teamsakata-img2.png" alt="">
								一人では不安だから一緒に事業計画から安定運営までサポートして欲しい。
							</li>
							<li>
								<img src="../assets/image/teamsakata/teamsakata-img2.png" alt="">
								障害福祉サービスを取り組みたい。
							</li>
							<li>
								<img src="../assets/image/teamsakata/teamsakata-img2.png" alt="">
								更生保護事業に興味がある。
							</li>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamsakata/teamsakata-img3.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamsakata2">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>心理カウンセリング</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>アイリッシュカフェ「心のオアシス」</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<h3>1人で抱えて、考えているだけでは堂々めぐり・・・</h3>
						<p>どうしていいのかわからない・・・</p>
						<p>落ち込んでいく・・・</p>
						<img src="../assets/image/teamsakata/teamsakata-img5.png" alt="">
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamsakata/teamsakata-img4.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamsakata3">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>お問い合わせ先</h1>
				</div>
				<div class="c-table1">
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>電話番号</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>03-3323-2495</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="c-table1__tr">
						<div class="c-table1__tr__left">
							<h1>メールアドレス</h1>
						</div>
						<div class="c-table1__tr__right">
							<p>team-sakata@w6.dion.ne.jp</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamsakata4">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>求人案内</h1>
				</div>
				<div class="c-text1">
					<p>
						テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamsakata5">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>アクセスマップ</h1>
				</div>
				<div class="c-mapBlock">
					<div class="c-map1">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
					</div>
					<a href="https://www.google.com/maps/place/1+Chome-13-9+Hanegi,+Setagaya-ku,+T%C5%8Dky%C5%8D-to+156-0042,+Japan/@35.6666557,139.6583409,17.46z/data=!4m5!3m4!1s0x6018f30e11f17561:0x9bb452e4b9254772!8m2!3d35.6667726!4d139.6595545?hl=en-US">詳しい地図を見る</a>
					<div class="c-list1">
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王線「代田橋」駅　徒歩6分
						</li>
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王井の頭線「新代田」駅　徒歩9分
						</li>
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王井の頭線「東松原」駅   徒歩9分　
						</li>
						<br>
						<li>※家庭料理のおざわが一階にあります。</li>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="../assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
