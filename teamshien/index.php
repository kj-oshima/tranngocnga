<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>


<div class="c-maintitle1">
	<img src="../assets/image/teamshien/maintitle6.png" alt="">
	<div class="c-maintitle1__text">
		<h1>一般社団法人team shien</h1>
		<h2>About</h2>
	</div>
</div>

<div class="c-breadcrumbs">
	<div class="c-breadcrumbs__text">
		<li>
			<a href="#">HOME</a>
			> 一般社団法人team shien
		</li>
	</div>
</div>

<div class="p-teamshien p-content1">
	<div class="p-teamshien1">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>介護保険ケアプラン（事業所番号 1371212207）</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>team shien</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p>看護師・社会福祉士・精神保健福祉士・介護福祉士の資格を持った福祉のスペシャリストが介護保険利用のためのケアプランを作成を承ります。</p>
						<h6>介護保険の利用に関わらず、福祉医療全般のご相談を無料にて承ります。</h6>
						<div class="c-info2">
							<h1>お問い合わせ先</h1>
							<p>月～土曜日 9:00～18:00 / TEL.03-6379-4343</p>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/teamshien-img1.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamshien2">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>訪問介護（事業所番号 1371209469）</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>ちーむしえんあどぼかしー</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p>介護員がお宅を訪問してサービスを提供させて頂きます。介護保険外の自費サービスも行っております。</p>
						<p>まずは、お気軽にお問い合わせください。</p>
						<div class="c-info2">
							<div class="c-info2--color2">
								<h1>お問い合わせ先</h1>
								<p>月～金曜日 9:00～18:00 / TEL.03-6379-5484</p>
							</div>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/teamshien-img3.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamshien3">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>一般相談・特定相談支援（事業所番号 1331202471）</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>team shien m.a</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<div class="c-title6">
							<h2>地域移行</h2>
							<h2>地域定着</h2>
						</div>
						<br>
						<p>障害をお持ちの方で、長期入院・入所をなさっている方の地域生活への移行・定着をお手伝いしています。</p>
						<p>※地域の行政担当・病院・施設の相談員の方を通じてお問い合わせください。</p>
						<br>
						<p>障害福祉サービスの利用を希望されている方のサービス等利用計画作成を承ります。</p>
						<p>例）訪問介護の利用や、就労支援先への通所など</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/teamshien-img4.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamshien4">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>居宅介護・移動支援（事業所番号 1311202467）</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>team shien m.a</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p>ちーむしえんあどぼかしーの訪問介護同様、専門の知識を有した介護員がケアに伺います。</p>
						<p>まずは、お気軽にお問い合わせください。</p>
						<br>
						<p>障害福祉サービス受給者証をお持ちの方は、介護給付費の１割負担でご利用いただけます。</p>
						<p>（所得に応じて負担上限金額が異なります）</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/teamshien-img5.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamshien5">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>一般相談・特定相談支援（事業所番号 1331202471））</h1>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/teamshien-img2.png" alt="">
					<p>ちーむしえん 研修部</p>
					<div class="clearfix"></div>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<div class="c-title6">
							<h2>介護職員初任者研修</h2>
							<h2>同行援護従事者研修</h2>
						</div>
						<br>
						<p>社会人でも取得しやすいプログラム、通信制を導入しております</p>
						<p>当社講師が誠心誠意、次世代の福祉人材を育て、社会へ貢献いたします。</p>
						<p>受講される方の意向を反映したオーダーメイド制研修を実施。</p>
						<p>6名以上の人数が揃い次第、随時研修を開始いたします。</p>
						<div class="c-info2">
							<h1>お問い合わせ先</h1>
							<p>月～土曜日 9:00～18:00 / TEL.03-6379-4343</p>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/teamshien-img6.png" alt="">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamshien6">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>お問い合わせ先</h1>
				</div>
				<div class="c-table1--c-color2">
					<div class="c-table1--c-color2__tr">
						<div class="c-table1--c-color2__tr__left">
							<h1>電話番号</h1>
						</div>
						<div class="c-table1--c-color2__tr__right">
							<p>03-6379-5484</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="c-table1--c-color2__tr">
						<div class="c-table1--c-color2__tr__left">
							<h1>メールアドレス</h1>
						</div>
						<div class="c-table1--c-color2__tr__right">
							<p>irishcafe@ab.auone-net.jp</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamshien7">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<h1>求人案内</h1>
				</div>
				<div class="c-text2">
					<p>随時、スタッフを募集しております。詳細については、お電話にてお尋ねください。</p>
					<br>
					<p>【募集職種】</p>
					<p>①ホームヘルパー</p>
					<p>②ケアマネージャー</p>
					<p>③相談支援専門員</p>
				</div>
			</div>
		</div>
	</div>

	<div class="p-teamshien8">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<h1>アクセスマップ</h1>
				</div>
				<div class="c-mapBlock">
					<div class="c-map1">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342773!2d139.6573658155701!3d35.66677693837128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1503037614438" frameborder="0" style="border:0" allowfullscreen=""></iframe>
					</div>
					<a href="https://www.google.com/maps/place/1+Chome-13-9+Hanegi,+Setagaya-ku,+T%C5%8Dky%C5%8D-to+156-0042,+Japan/@35.6666557,139.6583409,17.46z/data=!4m5!3m4!1s0x6018f30e11f17561:0x9bb452e4b9254772!8m2!3d35.6667726!4d139.6595545?hl=en-US">詳しい地図を見る</a>
					<div class="c-list1">
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王線「代田橋」駅　徒歩6分
						</li>
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王井の頭線「新代田」駅　徒歩9分
						</li>
						<li>
							<img src="../assets/image/teamsakata/teamsakata-img1.png" alt="">
							京王井の頭線「東松原」駅   徒歩9分　
						</li>
						<br>
						<li>※家庭料理のおざわが一階にあります。</li>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="../assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
