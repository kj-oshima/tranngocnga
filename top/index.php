<div class="p-top">
	<div class="p-top1">
		<div class="c-mainvisual">
			<img src="../assets/image/top/mainvisual.png" alt="">
		</div>
	</div>

	<div class="p-top2">
		<div class="u-bg1">
			<div class="l-container">
				<div class="c-title100">
					<div class="c-btn1">
						<a href="#">Facebook</a>
					</div>
					<div class="c-title8">
						<h1>お知らせ</h1>
						<p>News</p>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="c-list2">
					<ul>
						<li><img src="assets/image/top/img1.png" alt=""><span>2017/11/25</span>イベント情報ページを更新しました。</li>
					</ul>
					<ul>
						<li><img src="assets/image/top/img1.png" alt=""><span>2017/10/20</span>イベント情報ページを更新しました。</li>
					</ul>
					<ul>
						<li><img src="assets/image/top/img1.png" alt=""><span>2017/09/25</span>ホームページを開設いたしました。今後とも、チームエガオグループをよろしくお願いいたします。</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="p-top3">
		<div class="l-container">
			<div class="c-title1">
				<h1>チームエガオグループ（T.E.G Team Egao Group）</h1>
				<p>About</p>
			</div>
			<div class="c-nav2">
				<div class="c-nav2__tr">
					<div class="c-nav2__tr__colum">
						<div class="c-nav2__tr__colum__back">
							<img src="assets/image/top/img7.png" alt="">
						</div>
						<div class="c-nav2__tr__colum__text">
							<a href="#">
								<img src="assets/image/top/img2.png" alt="">
								<p>株式会社team sakata</p>
							</a>
						</div>
					</div>
					<div class="c-nav2__tr__colum">
						<div class="c-nav2__tr__colum__back">
							<img src="assets/image/top/img8.png" alt="">
						</div>
						<div class="c-nav2__tr__colum__text">
							<a href="#">
								<img src="assets/image/top/img3.png" alt="">
								<p>一般社団法人SHIEN</p>
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="c-nav2__tr">
					<div class="c-nav2__tr__colum">
						<div class="c-nav2__tr__colum__back">
							<img src="assets/image/top/img9.png" alt="">
						</div>
						<div class="c-nav2__tr__colum__text">
							<a href="#">
								<img src="assets/image/top/img4.png" alt="">
								<p>一般社団法人team shien</p>
							</a>
						</div>
					</div>
					<div class="c-nav2__tr__colum">
						<div class="c-nav2__tr__colum__back">
							<img src="assets/image/top/img10.png" alt="">
						</div>
						<div class="c-nav2__tr__colum__text">
							<a href="#">
								<img src="assets/image/top/img5.png" alt="">
								<p>NPO法人 ライフサポートさくら</p>
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-top4">
		<div class="u-bg2">
			<div class="l-container">
				<div class="c-info3">
					<div class="c-info3--left">
						<h1>チームエガオグループで一緒に働きませんか？</h1>
						<p>ご興味のある方はお電話にてご連絡ください。</p>
						<img src="assets/image/top/img12.png" alt="">
					</div>
					<div class="c-info3--right">
						<li>
							<img src="assets/image/top/img13.png" alt="">
							<span class="padd-right">
								株式会社 team sakata
							</span>
						</li>
						<li>
							<img src="assets/image/top/img14.png" alt="">
							<span class="padd-right">
								一般社団法人 SHIEN
							</span>
						</li>
						<li>
							<img src="assets/image/top/img15.png" alt="">
							<span>
								一般社団法人  team shien
							</span>
						</li>
						<li>
							<img src="assets/image/top/img16.png" alt="">
							<span class="padd-right">
								NPO法人  ライフサポートさくら
							</span>
						</li>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="c-gotop">
	<button onclick="topFunction()" id="myBtn" title="Go to top">
		<i><img src="../assets/image/top/img11.png" alt=""></i>
	</button>

	<script>
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</div>
